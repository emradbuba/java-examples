package com.emradbuba.learning.workout.leetcode.linkedlistcycle;

public interface LinkedListCycleSolution {
    boolean hasCycle(ListNode head);
}
