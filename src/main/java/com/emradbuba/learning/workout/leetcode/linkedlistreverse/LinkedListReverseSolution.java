package com.emradbuba.learning.workout.leetcode.linkedlistreverse;

public interface LinkedListReverseSolution {
    ListNode reverseList(ListNode head);
}