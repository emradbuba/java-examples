package com.emradbuba.learning.workout.leetcode.addtwonumbers;

public interface AddTwoNumbersSolution {
    ListNode addTwoNumbers(ListNode l1, ListNode l2);
}
